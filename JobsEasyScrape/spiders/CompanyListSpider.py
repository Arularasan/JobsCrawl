import scrapy
from scrapy.selector import Selector


class CompanyListSpider(scrapy.Spider):
    name = "companylist"

    start_urls = ["https://en.wikipedia.org/wiki/List_of_Indian_IT_companies"]

    def parse(self, response):
        listSelector = Selector(response)
        companyList = listSelector.xpath('//*[@id="mw-content-text"]/div/table//tr')
        with open("companylist.txt", "w") as f:
            for company in companyList:
                name = "".join(company.xpath('td[1]/a[contains(@href,"wiki/")]/text()').extract())
                wikiPage = "".join(company.xpath('td[1]/a/@href').extract())
                wikiPage = wikiPage[6:]
                if name != "" or wikiPage != "":
                    data = str(name) + ' : ' + str(wikiPage)
                    f.write(data + '\n')