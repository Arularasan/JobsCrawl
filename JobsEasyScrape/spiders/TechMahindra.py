import scrapy
from bs4 import BeautifulSoup

class TechMahindraSpider(scrapy.Spider):
    name = 'techmahindra'

    start_urls = ['https://careers.techmahindra.com/Forms/CurrentOpportunity.aspx']

    def parse(self, response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        print(bsObj.prettify())