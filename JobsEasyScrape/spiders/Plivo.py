import scrapy
from bs4 import BeautifulSoup

class Plivo(scrapy.Spider):
    name = 'plivo'

    start_urls = ['https://www.plivo.com/jobs/']

    def parse(self, response):
        bsObj = BeautifulSoup(response.text,'html.parser')
        for jobList in bsObj.findAll('div',class_='bb-public-jobs-list__deparment-section ptor-jobs-list__department-section'):
            for jobData in jobList.findAll('li'):
                job = {}
                job['title'] = jobData.find('a').contents[0]
                job['location'] = jobData.find('div').contents[0]