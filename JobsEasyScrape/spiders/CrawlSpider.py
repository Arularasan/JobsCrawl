import scrapy
from bs4 import BeautifulSoup
from bs4.element import Tag

class CrawlCompany(scrapy.Spider):
    name = 'crawlcomp'

    def start_requests(self):
        urls = ['http://quotes.toscrape.com/']
        for url in urls:
            yield scrapy.Request(url=url,callback=self.parse)

    def parse(self, response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        for child in bsObj.children:
            print(child)
            print(type(child))
            print('-------------')
            if type(child) is Tag:
                self.parseVal(child)

    def parseVal(self,child):
        print(child)
        print(type(child))
        print('--------------')
        if type(child) is Tag:
            self.parseVal(child)