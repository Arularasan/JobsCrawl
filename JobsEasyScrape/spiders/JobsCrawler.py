import scrapy
import sys
from bs4 import BeautifulSoup
from PyQt5.QtWebKitWidgets import QWebPage
from PyQt5.QtWidgets import QApplication

class Render(QWebPage):
    """Render HTML with PyQt5 WebKit."""

    def __init__(self, html):
        self.html = None
        self.app = QApplication(sys.argv)
        QWebPage.__init__(self)
        self.loadFinished.connect(self._loadFinished)
        self.mainFrame().setHtml(html)
        self.app.exec_()

    def _loadFinished(self, result):
        self.html = self.mainFrame().toHtml()
        self.app.quit()

class JobsCrawl(scrapy.Spider):
    name = 'jobcrawl'

    start_urls = ['http://quotes.toscrape.com/js']

    def parse(self, response):
        rendered_html = Render(response.text).html
        bsObj = BeautifulSoup(rendered_html,'html.parser')
        print(bsObj.prettify())