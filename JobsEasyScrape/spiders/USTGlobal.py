import requests
import json

true = True
false = False
pageno = 1


prevdata = []
while True:
    jsondata = {
        "multilineEnabled": false,
        "sortingSelection": {"sortBySelectionParam": "3", "ascendingSortingOrder": "false"},
        "fieldData": {"fields": {"KEYWORD": "", "LOCATION": ""}, "valid": true},
        "filterSelectionParam": {"searchFilterSelections": [{"id": "POSTING_DATE", "selectedValues": []},
                                                            {"id": "LOCATION", "selectedValues": []},
                                                            {"id": "JOB_FIELD", "selectedValues": []},
                                                            {"id": "JOB_TYPE", "selectedValues": []},
                                                            {"id": "JOB_SCHEDULE", "selectedValues": []},
                                                            {"id": "JOB_LEVEL", "selectedValues": []}]},
        "advancedSearchFiltersSelectionParam": {"searchFilterSelections": [{"id": "ORGANIZATION", "selectedValues": []},
                                                                           {"id": "LOCATION", "selectedValues": []},
                                                                           {"id": "JOB_FIELD", "selectedValues": []},
                                                                           {"id": "JOB_NUMBER", "selectedValues": []},
                                                                           {"id": "URGENT_JOB", "selectedValues": []},
                                                                           {"id": "EMPLOYEE_STATUS",
                                                                            "selectedValues": []},
                                                                           {"id": "WILL_TRAVEL", "selectedValues": []},
                                                                           {"id": "JOB_SHIFT", "selectedValues": []}]},
        "pageNo": pageno
    }
    data = requests.post(url='https://ustglobal.taleo.net/careersection/rest/jobboard/searchjobs?lang=en&portal=8115020170',json=jsondata).json()

    if not prevdata:
        print(type(prevdata))
        print(type(data['requisitionList']))
        if prevdata == data['requisitionList']:
            break
        else:
            for job in data['requisitionList']:
                print(job)
            prevdata = data['requisitionList']
    else:
        for job in data['requisitionList']:
            print(job)
        prevdata = data['requisitionList']
    pageno += 1