import scrapy
from scrapy_splash import SplashRequest

class WebParser(scrapy.Spider):
    name = 'webparser'

    #start_urls = ['https://ibegin.tcs.com/iBegin/jobs/search']

    def start_requests(self):
        urls = ['http://quotes.toscrape.com/js/']
        yield SplashRequest(url='http://quotes.toscrape.com/js/',callback=self.parse)

    def parse(self, response):
        for quote in response.css('div.quote'):
            yield {
                'text' : quote.css('span.text::text').extract_first()
            }
        print(response.text)