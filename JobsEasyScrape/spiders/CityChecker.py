import scrapy
from bs4 import BeautifulSoup

class CityFinder(scrapy.Spider):
    name = 'cityChecker'

    start_urls = ['https://en.wikipedia.org/wiki/List_of_cities_in_India_by_population']

    def parse(self, response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        for blockdata in bsObj.findAll('table',class_='wikitable sortable'):
            for rowData in blockdata.findAll('tr'):
                try:
                    city ={
                    'name' : "".join(rowData.findAll('td')[1].find('a').contents)
                    }
                    state = rowData.findAll('td')[4].find('a').contents
                    yield city
                except IndexError:
                    pass