import scrapy
from bs4 import BeautifulSoup
from scrapy_splash import SplashRequest
import json

class HclSpider(scrapy.Spider):
    name = 'hcl'

    COUNT = 1
    DIVCOUNT = 1

    #start_urls = ['https://www.hcltech.com/careers/Careers-in-india']

    def start_requests(self):
        yield SplashRequest(url='https://www.hcltech.com/careers/Careers-in-india',callback=self.parse)

    def parse(self, response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        divs = bsObj.findAll('div',class_='view-content')
        yield scrapy.Request(url='https://www.hcltech.com/views/ajax?view_name=list_jobs&view_display_id=block_1&view_args=India&view_path=node/206056&view_base_path=careers/all-career-opportunities&view_dom_id=39a1f520686691d61c5b6c4ae7bb79d3&pager_element=0&page=3&_=15031688787750',callback=self.parseAll)

    def parseAll(self,response):
        """html = response.body.json()[1]['data']
        bs = BeautifulSoup(html,'html.parser')
        print(bs.prettify())"""
        print(response.body)