import requests
import json
import ssl
from requests.exceptions import ConnectionError

class Tcs:
    PAGE_NUMBER = 1

    jsonPostdata = {
        "jobCity":None,
        "jobSkill":None,
        "pageNumber":str(PAGE_NUMBER),
        "userText" : "",
        "jobTitleOrder":None,
        "jobCityOrder":None,
        "jobFunctionOrder":None,
        "jobExperienceOrder":None,
        "applyByOrder":None,
        "regular":True,
        "walkin":True
    }

    def start_retrieve(self):
        try:
            while True:
                data = requests.post('https://ibegin.tcs.com/iBegin/api/v1/jobs/searchJ',json=self.jsonPostdata).json()['data']['jobs']
                if data:
                    for jobData in data:
                        job= {}
                        job['title'] = jobData['jobTitle']
                        job['companyname'] = 'TCS'
                        job['location'] = jobData['location']
                        print(job)
                if not data:
                    break
                self.PAGE_NUMBER += 1
                self.jsonPostdata['pageNumber'] = str(self.PAGE_NUMBER)
        except (ssl.SSLError, ConnectionError)  as e:
            pass

Tcs().start_retrieve()