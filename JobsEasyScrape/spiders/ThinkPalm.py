import scrapy
from bs4 import BeautifulSoup

class ThinkPalm(scrapy.Spider):
    name = 'thinkpalm'

    start_urls = ['http://thinkpalm.com/about-us/careers/']

    def parse(self, response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        print(bsObj.find('div',class_='sjb-wrap').prettify())