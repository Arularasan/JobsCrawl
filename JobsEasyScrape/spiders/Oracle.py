import scrapy
from bs4 import BeautifulSoup
import requests
import json

class OracleSpider(scrapy.Spider):
    name = 'oracle'

    start_urls = ['https://oracle.taleo.net/careersection/2/jobsearch.ftl']

    def parse(self, response):
        print(response.headers)