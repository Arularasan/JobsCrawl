import requests
import json

true = True
false = False
pageno = 1


headerData = {
"Host": "xerox.taleo.net",
"User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0",
"Accept": "application/json, text/javascript, */*; q=0.01",
"Accept-Language": "en-US,en;q=0.5",
"Accept-Encoding": "gzip, deflate, br",
"Content-Type": "application/json",
"tz": "GMT+05:30",
"X-Requested-With": "XMLHttpRequest",
"Referer": "https://xerox.taleo.net/careersection/xerox_shared_external_portal/jobsearch.ftl?lang=en",
"Content-Length": "840",
"Cookie": "locale=en; __atuvc=1%7C34; __atuvs=599ea5db97532dcf000",
"Connection": "keep-alive"
}
prevdata = {'requisitionList':''}
jobSet = set()
while True:
    data = requests.post(url='https://xerox.taleo.net/careersection/rest/jobboard/searchjobs?lang=en&portal=40140031924',json={"multilineEnabled":true,"sortingSelection":{"sortBySelectionParam":"3","ascendingSortingOrder":"false"},"fieldData":{"fields":{"KEYWORD":"","LOCATION":"","JOB_NUMBER":""},"valid":true},"filterSelectionParam":{"searchFilterSelections":[{"id":"POSTING_DATE","selectedValues":[]},{"id":"LOCATION","selectedValues":[]},{"id":"JOB_FIELD","selectedValues":[]},{"id":"JOB_TYPE","selectedValues":[]},{"id":"JOB_SCHEDULE","selectedValues":[]},{"id":"JOB_LEVEL","selectedValues":[]}]},"advancedSearchFiltersSelectionParam":{"searchFilterSelections":[{"id":"ORGANIZATION","selectedValues":[]},{"id":"LOCATION","selectedValues":[]},{"id":"JOB_FIELD","selectedValues":[]},{"id":"URGENT_JOB","selectedValues":[]},{"id":"EMPLOYEE_STATUS","selectedValues":[]},{"id":"STUDY_LEVEL","selectedValues":[]},{"id":"WILL_TRAVEL","selectedValues":[]}]},"pageNo":pageno},headers = headerData).json()

    if pageno>13:
        break
    else:
        for jobs in data['requisitionList']:
            if jobs['column'][2].split('-')[0][2:] == 'India':
                job = {}
                job['title'] = jobs['column'][0]
                job['location'] = jobs['column'][2].split('-')[2][:-2]
                print(job)
                jobSet.add(job)
    pageno += 1