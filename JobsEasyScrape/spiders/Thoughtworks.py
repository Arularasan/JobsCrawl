import scrapy
from bs4 import BeautifulSoup

class ThoughtWorks(scrapy.Spider):
    name = 'thoughtwork'

    start_urls = ['https://www.thoughtworks.com/careers/browse-jobs']

    def parse(self, response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        for data in bsObj.findAll('div',class_='job-list-city-group',attrs={'data-region' : 'India'}):
            city = data['data-city']
            for job in data.findAll('a'):
                jobData = {}
                jobData['title'] = job.contents[0]
                jobData['city'] = city