import scrapy
from scrapy.selector import Selector
import re
from JobsScrapy.items import JobsscrapyItem

companyList=list()
with open("companylist.txt",'r') as f:
    companyList=[line.rstrip('\n') for line in f]

class CompanySpider(scrapy.Spider):
    name = "company"

    COUNT=1


    def start_requests(self):
        urls=[]

        for companyLink in companyList:
            urls.append("https://en.wikipedia.org/wiki/%s" % companyLink.split(':')[1])

        for url in urls:
            yield scrapy.Request(url=url,callback=self.parse)


    def parse(self, response):
        companySelector=Selector(response)
        item = JobsscrapyItem()
        item['id'] = self.COUNT
        website = ""
        item['name']="".join(companySelector.xpath('//h1[@id="firstHeading"]/text()').extract())
        for row in companySelector.xpath('//div[@class="mw-parser-output"]/table[@class="infobox vcard"]//tr'):
            rowName = "".join(row.xpath("th/text()").extract())
            if rowName=="Website":
                website="".join(row.xpath("td/a/@href").extract())
            if website=='':
                item['Website']="".join(row.xpath("td/span/a/@href").extract())
            else:
                item['Website']=website
        description ="".join(companySelector.xpath('//div[@id="mw-content-text"]/div/p[1]').extract())
        tagremove = re.compile('<.*?>|\[.*\]')
        item['description'] = re.sub(tagremove, '', description)
        self.COUNT+=1
        yield item