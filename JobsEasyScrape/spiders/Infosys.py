import scrapy
from bs4 import BeautifulSoup
class InfosysSpider(scrapy.Spider):
    name = 'infosys'

    COUNT = 1

    CURRENTPAGE = 1

    start_urls = ['https://jobopenings.infosys.com/search/']

    def parse(self, response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        for data in bsObj.findAll('tr', class_='data-row clickable'):
            job = {}
            job['id'] = self.COUNT
            job['name'] = data.find('a',class_='jobTitle-link').contents[0]
            job['location'] = data.find('span',class_='jobLocation').contents[0].split(',')[0]
            print(job)
            self.COUNT +=1
        links = bsObj.find('ul',class_='pagination-links')
        for pagelink in links.findAll('a'):
            try:
                if (self.CURRENTPAGE+1) ==(int(pagelink.contents[0])):
                    yield scrapy.Request(url='https://jobopenings.infosys.com/search/'+pagelink['href'],callback=self.parse)
                    self.CURRENTPAGE += 1
                    break
            except ValueError:
                pass