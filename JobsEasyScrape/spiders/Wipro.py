import scrapy
from bs4 import BeautifulSoup

class WiproSpider(scrapy.Spider):
    name = 'wipro'

    start_urls = ['http://careers.wipro.com/search-for-jobs.aspx?ctr=IND&ct=0&exp=0&#divhere']

    def parse(self, response):
        """
        bsObj = BeautifulSoup(response.body,'html.parser')
        for data in bsObj.find('div',class_='jobBoxesList'):
            print(data)
        """
        yield scrapy.Request(
            url='http://careers.wipro.com/searchjoblist.aspx?ps=comapnyCode=WT|Opr=getWalkInAdvancedSearchResults|source=wiproCareerSite|country=0|location=0|expLevel=0|startIndx=1|endIndx=1784',
            callback=self.parseAll
        )

    def parseAll(self,response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        for data in bsObj.findAll('ul',class_='seacJobBox'):
            for list in data.findAll('li'):
                if list.findAll('p')[1].contents[0].split(',')[1]=='India  ':
                    job = {}
                    job['title'] = list.find('h4').contents[0]
                    job['location'] = list.findAll('p')[1].contents[0].split(',')[0]
                    print(job)