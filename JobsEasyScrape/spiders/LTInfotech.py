import requests
from bs4 import BeautifulSoup

class LTInfotech:

    def start(self):
        data = requests.get(url='http://rbase.lntinfotech.com/lnt_cp/client/Search.aspx').text

        bsObj = BeautifulSoup(data,'html.parser')
        for tablerow in bsObj.find('table',class_='SearchGrid').findAll('tr'):
            job = {}
            job['title'] = tablerow.find('a', class_='heading').contents[0]
            try:
                job['location'] = tablerow.find('p', class_='Location').find('span').contents[0].split('-')[1]
            except IndexError:
                job['location'] = ''
            print(job)

OBJ = LTInfotech()
OBJ.start()