import scrapy
from bs4 import BeautifulSoup

class HexawareSpider(scrapy.Spider):
    name = 'hexaware'

    start_urls = ['http://hexaware.com/careers/jobs/india/']

    def parse(self, response):
        bsObj = BeautifulSoup(response.body,'html.parser')
        for data in bsObj.find('div',class_='job_prt_wrap').findAll('div',class_='apply-job-part'):
            job = {}
            job['title'] = data.find('p').contents[0]
            for list in data.findAll('li'):
                if list.find('span', class_='location-image-space') is not None:
                    job['location'] = list.contents[1]
            print(job)