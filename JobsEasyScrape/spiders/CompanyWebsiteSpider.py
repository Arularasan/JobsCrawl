import scrapy
from bs4 import BeautifulSoup
import re

class NonAngularSite:
    def getLinks(self,bsObj,keywords):
        returnnSet = set()
        for word in keywords:
            search = '.*(',word,').*'
            listLinks = bsObj.findAll('a',{"href" : re.compile("".join(search))})
            for link in listLinks:
                returnnSet.add(link)
        return returnnSet

    def getAllLinks(self,bsObj):
        return bsObj.findAll('a')

    def parseData(self,bsObj,keywords):
        for data in bsObj.findAll(re.compile('job')):
            pass

class CompanyWebsite(scrapy.Spider):
    name = "companywebsite"

    visitedLinks = set()

    start_urls = ["https://www.thoughtworks.com/"]

    def parse(self, response):
        bs = BeautifulSoup(response.text,'html.parser')
        self.visitedLinks.add(response.url)
        keywords = ['job','career']
        site = NonAngularSite()
        jobLinks = site.getLinks(bs,keywords)
        if jobLinks.__len__() != 0 :
            for link in jobLinks not in self.visitedLinks:
                pass #scrape the links for information
        else:
            allLinks = site.getAllLinks(bs)
            for link in allLinks not in self.visitedLinks:
                pass #scrape for all the links not visited