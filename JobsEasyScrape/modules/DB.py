import sqlite3
from peewee import *

db = SqliteDatabase('JobsScrape.db')

class Jobs(Model):
    companyid = IntegerField()
    title = CharField()
    location = CharField()

    class Meta:
        database = db


db.connect()
#db.create_table(Jobs)

"""
job=Jobs()
job.companyid=1
job.title='Engineer'
job.location='Chennai'
job.save()


for job in Jobs.select():
    print(job.title)
"""