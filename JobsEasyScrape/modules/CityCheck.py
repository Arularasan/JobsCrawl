import json


class Checker:
    def getCityJson(self):
        with open('cities.json') as data:
            cityList = json.load(data)
        return cityList

    def checkCity(self,cityList,city):
        for cityName in cityList:
            if city.lower() == cityName.lower():
                return True