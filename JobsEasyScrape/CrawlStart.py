from JobsEasyScrape.spiders.Plivo import Plivo
from scrapy.crawler import CrawlerProcess
from scrapy.utils import log

process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
    'FEED_FORMAT': 'json',
    'FEED_URI': 'cities.json'
})

log.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
    'loggers': {
        'scrapy': {
            'level': 'ERROR',
        }
    }
})

process.crawl(Plivo)
process.start()
process.stop()

"""
root = Tk()

root.title('Jobs')

name = Entry(root)
name.pack()
name.focus_set()

send = Button(root,text='Send')
send.pack(side='bottom')
quit = Button(root,text='Quit',command=root.quit)
quit.pack(side='bottom')

root.mainloop()
"""